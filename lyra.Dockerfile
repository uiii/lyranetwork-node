FROM mcr.microsoft.com/dotnet/core/sdk:3.1

ARG LYRA_VERSION
ARG LYRA_NETWORK
ARG LYRA_LEXUSER_PASS
ARG LYRA_WALLET

ENV LYRA_VERSION ${LYRA_VERSION}
ENV LYRA_NETWORK ${LYRA_NETWORK}
ENV LYRA_LEXUSER_PASS ${LYRA_LEXUSER_PASS}
ENV LYRA_WALLET ${LYRA_WALLET}

RUN apt-get update && apt-get install -y wget bzip2 vim jq

RUN wget -O /tmp/lyra.tar.gz -q --show-progress --progress=bar:force:noscroll https://github.com/graft-project/LyraNetwork/releases/download/${LYRA_VERSION}/lyra.permissionless-${LYRA_VERSION}.tar.gz && \
    tar xvfj /tmp/lyra.tar.gz -C /opt

RUN mkdir /root/.Lyra

RUN jq ".ApplicationConfiguration.LyraNode.Lyra.NetworkId = \"${LYRA_NETWORK}\" | \
        .ApplicationConfiguration.LyraNode.Lyra.Database.DBConnect = \"mongodb://lexuser:${LYRA_LEXUSER_PASS}@lyra_mongo/lyra\" | \
        .ApplicationConfiguration.LyraNode.Lyra.Database.DexDBConnect = \"mongodb://lexuser:${LYRA_LEXUSER_PASS}@lyra_mongo/dex\" | \
        .ApplicationConfiguration.LyraNode.Lyra.Wallet.Name = \"${LYRA_WALLET}\"" \
        /opt/lyra/node/config.${LYRA_NETWORK}.json > tmp.json && mv tmp.json /opt/lyra/node/config.${LYRA_NETWORK}.json

RUN cat /opt/lyra/node/config.${LYRA_NETWORK}.json

RUN dotnet dev-certs https --clean && \
    dotnet dev-certs https

EXPOSE 4503
EXPOSE 4504
EXPOSE 4505

WORKDIR /opt/lyra/node

COPY scripts/lyra-startup.sh /opt/startup.sh

CMD /opt/startup.sh
