mongo -u $MONGO_INITDB_ROOT_USERNAME -p $MONGO_INITDB_ROOT_PASSWORD <<EOF
use lyra
db.createUser({user:'lexuser',pwd:'${LYRA_LEXUSER_PASS}',roles:[{role:'readWrite',db:'lyra'}]})
use dex
db.createUser({user:'lexuser',pwd:'${LYRA_LEXUSER_PASS}',roles:[{role:'readWrite',db:'dex'}]})
EOF
