#!/usr/bin/env bash

cp /opt/lyra/node/config.${LYRA_NETWORK}.json /root/.Lyra

[[ -f $HOME/Lyra-CLI-${LYRA_NETWORK}/poswallet.db ]] && cp $HOME/Lyra-CLI-${LYRA_NETWORK}/poswallet.db $HOME/.Lyra/Lyra-CLI-${LYRA_NETWORK}/poswallet.db

[[ ! -f $HOME/.Lyra/Lyra-CLI-${LYRA_NETWORK}/poswallet.db ]] && dotnet /opt/lyra/cli/lyracli.dll --networkid ${LYRA_NETWORK} -p webapi -g ${LYRA_WALLET}

cp /opt/lyra/node/protocol.${LYRA_NETWORK}.json /root/.Lyra/protocol.${LYRA_NETWORK}.json

dotnet /opt/lyra/node/Lyra.Node2.dll
