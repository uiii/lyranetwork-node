# LyraNetwork node

## Installation
1. Install docker: `sudo sh bin/install-docker.sh`
2. Set environment variables: `cp .env.example .env` and fill values in `.env`
3. Build docker containers: `sh bin/build.sh`
4. Start Lyra node: `sh bin/start.sh`

> To stop use `sh bin/stop.sh`

> **IMPORTANT**: If you change variables in `.env`, you have to rebuild docker containers (`sh bin/build.sh`) and restart Lyra node

## Useful scripts
- `bin/logs.sh` - show Lyra node log output
- `bin/lyra-shell.sh` - access Lyra node shell
