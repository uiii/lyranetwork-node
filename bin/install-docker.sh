#!/bin/bash

# install prerequisities
apt-get install -y apt-transport-https ca-certificates curl software-properties-common

# enable aufs filesystem
echo "aufs" >> /etc/modules
modprobe aufs
echo '{"storage-driver": "aufs"}' >> /etc/docker/daemon.json

# install docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install -y docker-ce

docker swarm init
